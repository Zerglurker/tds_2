// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "TDS_1CharacterHealthComponent.h"
#include "CoreMinimal.h"
#include "Engine/World.h"
#include "Character/TDS_1HealthComponent.h"

UTDS_1CharacterHealthComponent::UTDS_1CharacterHealthComponent() 
{

}

void UTDS_1CharacterHealthComponent::BeginPlay()
{
	Super::BeginPlay();

}

void UTDS_1CharacterHealthComponent::ChangeHealthValue(float ChangeValue)
{
	
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);

		if (Shield < 0.0f)
		{
			//FX
			//UE_LOG(LogTemp, Warning, TEXT("UTPSCharacterHealthComponent::ChangeHealthValue - Sheild < 0"));
		}
	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);
	}
}

float UTDS_1CharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTDS_1CharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
			Shield = 0.0f;
	}
	OnShieldChange.Broadcast(Shield, ChangeValue);

	if (GetWorld())
	{	//������� �������
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UTDS_1CharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}

}

void UTDS_1CharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UTDS_1CharacterHealthComponent::RecoveryShield, ShieldRecoverRate, true);
	}
}

void UTDS_1CharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoverValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}

	OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
}
