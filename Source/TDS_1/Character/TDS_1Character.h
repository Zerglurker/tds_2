// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FuncLibrary/Types.h"
#include "Weapons/WeaponDefault.h" 
#include "Character/TDS_1InventoryComponent.h"
#include "Character/TDS_1CharacterHealthComponent.h"
#include "Interface/TDS_IGameActor.h"
#include "StateEffects/TDS_1StateEffect.h"

#include "TDS_1Character.generated.h"

UCLASS(Blueprintable)
class ATDS_1Character : public ACharacter, public ITDS_IGameActor
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	//Inputs hooks
	void InputAxisX(float Value);
	void InputAxisY(float Value);

	void InputAttackPressed();
	void InputAttackReleased();

	void InputWalkPressed();
	void InputWalkReleased();

	void InputSprintPressed();
	void InputSprintReleased();

	void InputAimPressed();
	void InputAimReleased();

	//Inventory Inputs
	void TrySwitchNextWeapon();
	void TrySwitchPreviosWeapon();
	//ability func
	void TryAbilityEnabled();

	template<int32 Id> 
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput(Id);
	}
	void KeyPressed0();
	void KeyPressed1();
	void KeyPressed2();
	void KeyPressed3();
	void KeyPressed4();
	//Inputs End

	//Input Flags
	float AxisX = 0.0f;
	float AxisY = 0.0f;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool SprintEnabled = false;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool WalkEnabled = false;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool AimEnabled = false;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool bIsAlive = true;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	EMovementState MovementState = EMovementState::Run_State;

	//Weapon   
	AWeaponDefault* CurrentWeapon = nullptr;

	//new cursor
	UDecalComponent* CurrentCursor = nullptr;

	//Effect
	TArray<UTDS_1StateEffect*> Effects;

	//UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	int32 CurrentIndexWeapon = 0;

	UFUNCTION()
		void CharDead();
	void EnableRagdoll();
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

public:
	ATDS_1Character();

	FTimerHandle TimerHandle_RagDollTimer;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTDS_1InventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTDS_1CharacterHealthComponent* CharHealthComponent;

	//Cursor material on decal
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Cursor")
		FVector CursorSize = FVector(20.f,40.f,40.f);
	//Default move rule and state character
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
		float SprintWeakness = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float Stamina = 1000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float MaxStamina = 1000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		TArray<UAnimMontage*> DeadsAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UTDS_1StateEffect> AbilityEffect;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	void UpdateStamina(float DeltaTime);

	float DefAccelerate = 0.f;

	//Camera height
	void SmoothCamHeight(float DeltaTime);

	float SensetiveHeight = 1.0f;
	float HeightControl = 0.0f;
public:	
	//Camera height
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		float MinHeight = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		float MaxHeight = 1200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float Height = 1000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		float StagesHeight = 7.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		float CoffSmooth = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		float SpeedHeight = 500.0f;

	//Shield
	UPROPERTY(Category = "Shield", EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* ShieldBody = nullptr;

	UFUNCTION()
		void InputHeight(float Value);

public:	
	//Tick for movement character
	UFUNCTION()
		void MovementTick(float DeltaTime);
	//Func
	//UFUNCTION(BlueprintCallable)
	void CharacterUpdate();
	//UFUNCTION(BlueprintCallable)
	void ChangeMovementState();

	//UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFiring);

	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();
	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
	
	UFUNCTION(BlueprintCallable)
	bool TrySwitchWeaponToIndexByKeyInput(int32 ToIndex);
	void DropCurrentWeapon();
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		UDecalComponent* GetCursorToWorld();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		EMovementState GetMovementState();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		TArray<UTDS_1StateEffect*> GetCurrentEffectsOnChar();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		int32 GetCurrentWeaponIndex();

	UFUNCTION(BlueprintNativeEvent)
		void CharDead_BP();
	//Func End


	//Interface ITDS_IGameActor {
	virtual EPhysicalSurface GetSurfaceType();

	virtual TArray<UTDS_1StateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UTDS_1StateEffect* RemoveEffect);
	virtual void AddEffect(UTDS_1StateEffect* newEffect);

	virtual UStaticMeshComponent* GetShieldBody();
	//Interface ITDS_IGameActor }


};

