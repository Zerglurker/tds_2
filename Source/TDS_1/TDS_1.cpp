// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TDS_1.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_1, "TDS_1" );

DEFINE_LOG_CATEGORY(LogTDS_1)
 