// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/TDS_IGameActor.h"
#include "TDS_1EnvironmentStructure.generated.h"

/*
  �������� �����������/������������ �������� �� �����
*/
UCLASS()
class TDS_1_API ATDS_1EnvironmentStructure : public AActor, public ITDS_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDS_1EnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Interface ITDS_IGameActor {
	virtual EPhysicalSurface GetSurfaceType();

	virtual TArray<UTDS_1StateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UTDS_1StateEffect* RemoveEffect);
	virtual void AddEffect(UTDS_1StateEffect* newEffect);
	//Interface ITDS_IGameActor }

	//Effect
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		TArray<UTDS_1StateEffect*> Effects;

};
