// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_1EnvironmentStructure.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
ATDS_1EnvironmentStructure::ATDS_1EnvironmentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATDS_1EnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDS_1EnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ATDS_1EnvironmentStructure::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result;
}

TArray<UTDS_1StateEffect*> ATDS_1EnvironmentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void ATDS_1EnvironmentStructure::RemoveEffect(UTDS_1StateEffect * RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ATDS_1EnvironmentStructure::AddEffect(UTDS_1StateEffect * newEffect)
{
	Effects.Add(newEffect);
}

