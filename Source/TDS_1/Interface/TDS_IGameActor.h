// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Engine/EngineTypes.h"
#include "StateEffects/TDS_1StateEffect.h"
#include "FuncLibrary/Types.h"
#include "TDS_IGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class UTDS_IGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDS_1_API ITDS_IGameActor
{
	
	GENERATED_BODY()

public:	
	// Add interface functions to this class. This is the class that will be inherited to implement this interface.


	virtual EPhysicalSurface GetSurfaceType();

	virtual TArray<UTDS_1StateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UTDS_1StateEffect* RemoveEffect);
	virtual void AddEffect(UTDS_1StateEffect* newEffect);

	//BlueprintCallable,BlueprintNativeEvent
	UFUNCTION(BlueprintCallable,BlueprintNativeEvent) 
		void DropWeaponToWorld(FDropItem DropItemInfo);
	UFUNCTION(BlueprintCallable,BlueprintNativeEvent)
		void DropAmmoToWorld(EWeaponType TypeAmmo, int32 count);

	virtual UStaticMeshComponent* GetShieldBody();
};
