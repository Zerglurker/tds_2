// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
//#include "GameFramework/GameModeBase.h"
#include "GameFramework/GameMode.h"
#include "TDS_1GameMode.generated.h"

UCLASS(minimalapi)
class ATDS_1GameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ATDS_1GameMode();

	void PlayerCharacterDead();

};



