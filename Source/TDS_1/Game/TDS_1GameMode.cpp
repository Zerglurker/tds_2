// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TDS_1GameMode.h"
#include "TDS_1PlayerController.h"
#include "Character/TDS_1Character.h"
#include "UObject/ConstructorHelpers.h"

ATDS_1GameMode::ATDS_1GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDS_1PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Blueprint/Character/BP_Character")); // /Game/Blueprint/Character/BP_Character
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("ATDS_1GameMode::ATDS_1GameMode - PlayerPawnBPClass not found NULL!"));
	}
}

void ATDS_1GameMode::PlayerCharacterDead()
{
}
